FROM python:3.7-slim-bullseye

RUN mkdir /opt/veracode

COPY requirements.txt	/opt/veracode/requirements.txt

RUN pip install -r /opt/veracode/requirements.txt

COPY './veracode-bitbucket-pr-comment.py' /opt/veracode/veracode-bitbucket-pr-comment.py
