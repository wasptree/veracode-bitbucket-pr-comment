# Veracode Bitbucket Pull Request Comment
> This project is currently in developent

> This is not an official Veracode project

<!-- ABOUT THE PROJECT -->
## About This Project

veracode-bitbucket-pr-comment is python script to automate the process of adding a Veracode analysis summary output onto a Bitbucket pull request (PR) discussion board from the CI/CD pipeline.

This is to help developers and reviewers see security analysis results directly on the merge request discussion board.

This is not an official Veracode project.


<!-- GETTING STARTED -->
## Getting Started

Docker:
A pre-built amd64 versions of the veracode-glmc image containing the dependencies and python script is available on docker hub [hub.docker.com/veracode-bitbucket-pr-comment](https://hub.docker.com/repository/docker/wasptree/veracode-bitbucket-pr-comment)

Alternatively build a local image from this repository:

  ```sh
    $ git clone https://gitlab.com/wasptree/veracode-bitbucket-pr-comment
    $ cd veracode-bitbucket-pr-comment
    $ docker build -t veracode-bitbucket-pr-comment .
  ```

Python Script:
Alternatively just download and run the python script in your Bitbucket pipeline

<!-- SETUP -->
## Setup

For the script to be able authenticate to Bitbucket and comment on the current merge request, an ACCESS TOKEN is required.

1. Generate a valid Bitbucket token for your project
   ```
   Repository Settings -> Access Tokens -> Create Repository Access Token
   ```
2. Set the token as an environment variable, accessible to your Gitlab pipeline. *This variable must be called BITBUCKET_ACCESS_TOKEN*
   ```
   Repository Settings -> Repository Variables -> Add
   ```


<!-- USAGE EXAMPLES -->
## bitbucket-pipelines.yml

The following example shows using the Veracode pipeline scan agent to perform a SAST analysis and save a summary output to the file summary_report.txt. 

This summary report is then converted and noted on the merge request.

Checkout the bitbucket-pipelines.yml file example in this project.
```
    - step:
        name: 'Veracode Pipeline Scan'
        script:
          - curl -O https://downloads.veracode.com/securityscan/pipeline-scan-LATEST.zip
          - unzip pipeline-scan-LATEST.zip pipeline-scan.jar
          - java -jar pipeline-scan.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY} --file "./target/verademo.war" -so true -sf summary_report.txt || true
        artifacts:
          paths:
            - summary_report.txt
      
    - step:
        name: 'Pull Request Decoration'
        image: wasptree/veracode-bitbucket-pr-comment
        script: 
          - python3 /opt/veracode/veracode-bitbucket-pr-comment.py --filename summary_report.txt
```
## Screenshot:
![](/images/2023-03-24-17-47-37.png)