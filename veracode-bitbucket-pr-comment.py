import os
import argparse
import requests
import re

parser = argparse.ArgumentParser(description='Veracode summary report to comment on a Bitbucket pull request')
parser.add_argument("-f", "--filename", help="The summary report to comment")
args = parser.parse_args()
filename = args.filename
if not os.path.exists(filename):
    print(" [!] Report file is not accessible. Write Veracode summary output to file")
    exit(1)

# Get the Bitbucket access token from the environment variable
try:
    access_token = os.environ['BITBUCKET_ACCESS_TOKEN']
except:
    print(" [!] BITBUCKET_ACCESS_TOKEN environment variable is not set")
    exit(1)

# Get the pull request's ID
try:
    pr_id = os.environ['BITBUCKET_PR_ID']
except:
    print(" [!] BITBUCKET_PR_ID is not set. Is this a pull request?")
    exit(1)

try:
    workspace = os.environ['BITBUCKET_WORKSPACE']
except:
    print(" [!] BITBUCKET_WORKSPACE is not set. Is this a pipeline?")
    exit(1)

try:
    repo_slug = os.environ['BITBUCKET_REPO_SLUG']
except:
    print(" [!] BITBUCKET_REPO_SLUG is not set. Is this a pipeline?")
    exit(1)

# Set up the Bitbucket API headers with the access token
headers = {
    'Authorization': f'Bearer {access_token}',
    'Content-Type': 'application/json'
}

# Set up the Bitbucket API endpoint for the pull request's comments
api_url = f'https://api.bitbucket.org/2.0/repositories/{workspace}/{repo_slug}/pullrequests/{pr_id}/comments'

# Read in the Veracode Summary output and reformat it for Bitbucket flavour of Markdown
with open(filename, 'r') as f:
    analysis = f.read()
    analysis = "![](https://gitlab.com/wasptree/veracode-bitbucket-pr-comment/-/raw/master/images/veracode-black-hires.svg){width=300 height=100px}" + analysis
    analysis = '## Veracode Summary Report\n\n' + analysis
    analysis = analysis.replace('\n', '  \n')
    analysis = re.sub(r'===+', '', analysis)
    analysis = re.sub(r'---+', '\n\n---', analysis)
    analysis = re.sub(r'Scan Summary:', '## Scan Summary:', analysis)
    analysis = re.sub(r'\nAnaly', '\n## Analy', analysis)
    analysis = re.sub(r'\nFound', '\n### Found', analysis)
    analysis = re.sub(r'\nSkipping', '\n### Skipping', analysis)
    analysis = re.sub(r'Analyzed (\d+) issues.', r'Identified \1 Flaws.\n\n', analysis)
    analysis = re.sub(r'FAILURE: Found (\d+) issues!', r'\n\n### :warning: POLICY FAILURE: \1 flaws are failing policy', analysis)
    analysis = re.sub(r'SUCCESS:', r'n\n### :white_check_mark: POLICY SUCCESS:', analysis)
    analysis = re.sub(r'CWE-(\d+)', r'[CWE-\1](https://cwe.mitre.org/data/definitions/\1.html)', analysis)

# Create the comment on the pull request using the Bitbucket API
payload = {
    'content': {
        'raw': analysis
    }
}
response = requests.post(api_url, headers=headers, json=payload)
print(response.request)

if response.status_code == 201:
    print(f' [+] Successfully commented on Bitbucket pull request {pr_id}')
else:
    print(f' [!] Failed to comment on Bitbucket pull request {pr_id}: {response.text}')
    exit(1)
